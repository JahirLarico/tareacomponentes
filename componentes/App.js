
import React, { Component } from 'react';
import { StyleSheet, Text, View ,SafeAreaView} from 'react-native';
import AgeValidator from './app/componentes/AgeValidator';
import Lista from './app/componentes/MyList';

export default class App extends Component {

  render() {
    return (
      <SafeAreaView style={styles.container}>

      <AgeValidator/>
      <Lista/>
      </SafeAreaView>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    paddingTop:100,
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal:10,
  },
  text:{
    alignItems: 'center',
    padding: 10,
  },
  button: {
    top: 10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF'
  }
});