import React from 'react';
import { SafeAreaView, View, FlatList, StyleSheet, Text, StatusBar, Image } from 'react-native';
const Lista = () => {
  const DATA = [
    {
      id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
      title: 'INFO DEL PRIMER IMTE',
      img: "https://blog.ida.cl/wp-content/uploads/sites/5/2020/04/tamano-redes-blog-655x470.png"
    },
    {
      id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
      title: 'INFO DEL SEGUNDO ITEM',
      img: "https://img.freepik.com/vector-gratis/frontera-fuego-verde-realista-imagenes-predisenadas-llama-ardiente_107791-1038.jpg?w=2000"
    },
    {
      id: '58694a0f-3da1-471f-bd96-145571e29d72',
      title: 'INFO DEL TERCER ITEM',
      img: "https://cdn.hobbyconsolas.com/sites/navi.axelspringer.es/public/styles/1200/public/media/image/2019/04/saw.jpg?itok=vQhZl1j9"
    },
  ];


  const renderItem = ({ item }) => (
    <View style={styles.item}>
    <Image source={{uri: item.img}} style={{width: 50, height: 50}}/>
    <Text>{item.title}</Text>
  </View>
  );

  return (
    <SafeAreaView style={styles.container}>
      <FlatList
        data={DATA}
        renderItem={renderItem}
      />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight || 0,
  },
  item: {
    backgroundColor: '#f9c2ff',
    padding: 20,
    marginVertical: 8,
    marginHorizontal: 16,
  },
  title: {
    fontSize: 32,
  },
});

export default Lista;